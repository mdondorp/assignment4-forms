/*global require, exports, console, __filename, sort */
/**
 * Created by theotheu on 24-12-13.
 */

var mongoose = require('mongoose'),
    Group = mongoose.model('Group');


// CREATE
// save @ http://mongoosejs.com/docs/api.html#model_Model-save
exports.create = function (req, res) {
    "use strict";

    var doc = new Group(req.body);

    doc.save(function (err) {

        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        console.log('Result ---', retObj);
        return res.send(retObj);
    });

};

// RETRIEVE
// find @ http://mongoosejs.com/docs/api.html#model_Model.find
exports.list = function (req, res) {
    "use strict";

    var sort, conditions, fields;

    conditions = {};
    fields = {};
    sort = {'modificationDate': -1};

    Group
        .find(conditions, fields, sort)
        .sort(sort)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};

exports.detail = function (req, res) {
    "use strict";

    var conditions, fields, options;

    conditions = req.params._id;
    fields = {};
    options = {'createdAt': -1};

    /** TODO: Replace "find" with "findById". Tip: you have to change the conditions as well.
     *  @see http://mongoosejs.com/docs/api.html#model_Model.findById
     */
    Group
        .findById(conditions, fields,  options)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};

// UPDATE
// findOneAndUpdate @ http://mongoosejs.com/docs/api.html#model_Model.findOneAndUpdate
exports.update = function (req, res) {
    "use strict";

    console.log('UPDATE group.');

    // Make sure that password is hashed.

    var conditions = req.params._id,
        update = {
            name: req.body.name,
            description: req.body.description || '',
            modificationDate: Date.now()
        },
        options = {},
        callback = function (err, doc) {

            // Like to see the updated document.
            // We de a nested findOne
            //Group
            var retObj = {
                meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);

        };

    Group
        .findByIdAndUpdate(conditions, update, options, callback);
};

// DELETE
// remove @ http://mongoosejs.com/docs/api.html#model_Model-remove
exports.delete = function (req, res) {
    "use strict";

    var conditions, retObj;

    conditions = {_id: req.params._id};

    Group
        .remove(conditions, function (err, doc) {
            retObj = {
                meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};
